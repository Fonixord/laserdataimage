﻿/*This file is part of LaserDataImage
Copyright (C) 2021 Hugo Johansson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.*/

using System.Linq;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.IO;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.NamingConventionBinder;
using System.Drawing;
using System.Globalization;
using CsvHelper;
using CsvHelper.Configuration;
using Newtonsoft.Json;
using netDxf;
using netDxf.Entities;
using LaserDataImage.classes;

namespace LaserDataImage
{
    //Libgdiplus, liblas and liblas-tools (providing las2las and las2txt) must be installed (Linux packages in bin)
    //Requires python for some images
    static class Program
    {
        private const string LidarFilesDir = "lidarFiles"; //Path for batch processing

        private enum ImageTypes
        {
            @class,
            groundElevation,
            layer,
            contourLines,
            classElevation,
            intensity,
            slopeDown,
            slopeLeft,
            slopeSum,
            vegetationElevation,
            slopeElevation,
            unidentifiedSlope,
            all
        }

        private enum GridType
        {
            GroundElevation,
            Intensity,
            VegetationElevation,
            unidentifiedElevation
        }

        private enum SlopeDirection
        {
            Down,
            Left,
            Sum
        }

        static int Main(string[] args)
        {
            RootCommand rootCommand = new RootCommand("Create images and DXF files from LiDAR data using LaserDataImage.");
            Command makeCommand = new Command("make", "Create an image or DXF file using options.")
            {
                new Argument<ImageTypes>(
                    "imageType",
                    description: "The type of image to create.") {Arity = ArgumentArity.ExactlyOne},
                new Argument<string>(
                    "inputPath",
                    description: "Path to .laz/.las file") {Arity = ArgumentArity.ExactlyOne},
                new Option<int[]>(
                    "--area",
                    description: "Area to create image from. Input x-min, y-min, x-max and y-max. Ex: '500 500 1000 1000' If not specified, the whole dataset is used.")
                    {AllowMultipleArgumentsPerToken=true},
            };

            makeCommand.Handler = CommandHandler.Create<ImageTypes, string, int[]>(
                (imageType, inputPath, area) =>   //These must match the option names
                {
                    Console.WriteLine("Number of coordinates: " + area.Length);
                    if (area.Length == 4)
                        MakeImageArgs(imageType, inputPath, area[0], area[1], area[2], area[3]);
                    else if (area.Length == 0)
                        RunProgram(imageType, inputPath, 0, 0, 0, 0);
                    else
                    {
                        Console.WriteLine("Invalid area coordinates. Should be x-min, y-min, x-max and y-max integers. Ex: '500 500 1000 1000'");
                        return;
                    }
                    
                }
            );
            
            rootCommand.Add(makeCommand);
            
            return rootCommand.InvokeAsync(args).Result;

            //i: Path to input LiDAR file.
            //folder: Batch process whole folder.
            //class: Draw the image based on class data
            //elevation: Draw the image based on elevation data
            //layer: Draw the image based on specific layers.
            //contours: Draw the ground data as contours.
            //unified: Draw buildings and water on top of elevation.
            //intensity: Draw the image based on intensity values.
            // Create a root command with some options
        }

        private static void RunProgram(ImageTypes imageType, string inputPath, int selXMin, int selYMin, int selXMax, int selYMax)
        {
            if (imageType == ImageTypes.all)    //Make all image types
            {
                foreach (ImageTypes imgType in Enum.GetValues(typeof(ImageTypes)))
                {
                    MakeImageArgs(imgType, inputPath, selXMin, selYMin, selXMax, selYMax);
                }
            }
            else
            {   //Make only selected image type
                MakeImageArgs(imageType, inputPath, selXMin, selYMin, selXMax, selYMax);
            }
        }

        /// <summary>
        /// Create on image (bitmap) from data points based on args.
        /// </summary>
        /// <param name="imgType">Image type to be drawn.</param>
        /// /// <param name="lidarPath">File name.</param>
        private static void MakeImageArgs(ImageTypes imgType, string lidarPath, int selXMin, int selYMin, int selXMax, int selYMax)
        {
            //Get datapoints
            if (lidarPath == "")
            {
                lidarPath = LidarFilesInDir(LidarFilesDir)[0];  //Only get first
            }
            DataPoint[] dataPoints = ProcessFile(lidarPath);
            //Find highest/lowest values
            FindMinMax(dataPoints, out double xMin, out double yMin, out double zMin, out int _, out double xMax, out double yMax, out double zMax, out int _);
            SelectedArea selectedArea = new SelectedArea(xMin, yMin, xMax, yMax, selXMin, selYMin, selXMax, selYMax);
            //Length on x-axis or y-axis
            double zRange = zMax - zMin; //Calculate range for z-values
            Console.WriteLine($"Z range for all data: {zRange}");

            //Find most amount of point on the x-axis if 
            Console.WriteLine("[30]");

            Bitmap image = new Bitmap(selectedArea.XDistance, selectedArea.YDistance);  //Create image which fits the maximum amount of points
            //Used in coloring to fill the entire image.
            switch (imgType)
            { //Call the appropriate method for the image type
                case ImageTypes.@class:
                    Console.WriteLine("Making class image");
                    PixelClassColoring(dataPoints, ref image, xMin, yMin, selectedArea);
                    break;
                case ImageTypes.groundElevation:
                    Console.WriteLine("Making ground elevation image");
                    PixelElevationColoring(dataPoints, ref image, lidarPath, selectedArea);
                    break;
                case ImageTypes.layer:
                    Console.WriteLine("Making layer image");
                    PixelLayerColoring(dataPoints, ref image, lidarPath, selectedArea);
                    break;
                case ImageTypes.contourLines:
                    Console.WriteLine("Making contour lines");
                    PixelContourDrawing(dataPoints, ref image, lidarPath, selectedArea);
                    break;
                case ImageTypes.classElevation:
                    Console.WriteLine("Making class-elevation image");
                    PixelUnifiedElevationColoring(dataPoints, ref image, lidarPath, selectedArea);
                    break;
                case ImageTypes.intensity:
                    Console.WriteLine("Making intensity image");
                    PixelIntensityColoring(dataPoints, ref image, lidarPath, selectedArea);
                    break;
                case ImageTypes.slopeDown:
                    Console.WriteLine("Making down-slope image");
                    PixelSlopeColoring(dataPoints, ref image, lidarPath, selectedArea, SlopeDirection.Down, GridType.GroundElevation);
                    break;
                case ImageTypes.slopeLeft:
                    Console.WriteLine("Making left-slope image");
                    PixelSlopeColoring(dataPoints, ref image, lidarPath, selectedArea, SlopeDirection.Left, GridType.GroundElevation);
                    break;
                case ImageTypes.slopeSum:
                    Console.WriteLine("Making slope-sum image");
                    PixelSlopeColoring(dataPoints, ref image, lidarPath, selectedArea, SlopeDirection.Sum, GridType.GroundElevation);
                    break;
                case ImageTypes.vegetationElevation:
                    Console.WriteLine("Making vegetation-elevation image");
                    PixelVegetationElevationColoring(dataPoints, ref image, lidarPath, selectedArea);
                    break;
                case ImageTypes.slopeElevation:
                    Console.WriteLine("Making slope-elevation image");
                    PixelSlopeElevationColoring(dataPoints, ref image, lidarPath, selectedArea, SlopeDirection.Sum);
                    break;
                case ImageTypes.unidentifiedSlope:
                    Console.WriteLine("Making unidentified-slope image");
                    PixelSlopeColoring(dataPoints, ref image, lidarPath, selectedArea, SlopeDirection.Sum, GridType.unidentifiedElevation);
                    break;
                default: //Should never happen, as enumeration is used
                    Console.WriteLine("Invalid input.");
                    break;
            }
            saveImage(image, lidarPath, imgType, xMin, xMax, yMin, yMax, selectedArea);
        }

        private static void saveImage(Bitmap image, string pathToFile, ImageTypes imageType, double xMin, double xMax, 
            double yMin, double yMax, SelectedArea selectedArea)
        {
            //Flip and save image
            image.RotateFlip(RotateFlipType.RotateNoneFlipY);
            string baseFileName = Path.GetFileNameWithoutExtension(pathToFile);
            DirectoryInfo workingDirectory = Directory.GetParent(pathToFile);
            if (!Directory.Exists(workingDirectory + "/output"))
                Directory.CreateDirectory(workingDirectory + "/output");
            string imagePath = workingDirectory + "/output/" + baseFileName + "_" + imageType.ToString() + ".png";
            image.Save(imagePath);
            Console.WriteLine("Image written to: " + imagePath);
            MakeWorldFile(imagePath, xMin, xMax, yMin, yMax, selectedArea.XDistance, selectedArea.YDistance);
            Console.WriteLine("[100]"); //Image finished processing
        }
        
        
        /// <summary>
        /// Call for each lidar file to process.
        /// </summary>
        /// <param name="lidarFiles">List of files (paths) to process</param>
        /// <param name="namesWithoutExt">List of files (paths) without extensions.</param>
        /// <param name="currentIteration">Current file which is being processed.</param>
        private static DataPoint[] ProcessFile(string lidarFilePath)
        {
            string txtPath = lidarFilePath.Remove(lidarFilePath.Length - 3) + "txt";
            //If there does not exists an ASCII formatted file (from a previous run), then convert the binary las/.laz to an ASCII .txt file.
            if (!File.Exists(lidarFilePath))
            {
                Console.WriteLine("LiDAR file not found.");
                Environment.Exit(1);
            } else if (!File.Exists(txtPath))
            {
                ConvertBinaryToXyzci(lidarFilePath, txtPath); //Will wait until finished
                Console.WriteLine("[20]");
            }
            //Read data from file and store each entry (data point as string) separately in an array
            CsvConfiguration config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = false,
            };
            using (var reader = new StreamReader(txtPath))
            using (var csv = new CsvReader(reader, config))
            {
                IEnumerable<DataPoint> dataPoints = csv.GetRecords<DataPoint>();
                return dataPoints.ToArray();
            }
        }

        /// <summary>
        /// Create a png world file (pgw) corresponding to the image, for georeferencing in other programs.
        /// </summary>
        /// <param name="imagePath">Path to image, for path to world file.</param>
        /// <param name="xMin">Minimum x value of the data points.</param>
        /// <param name="xMax">Maximum x value of the data points.</param>
        /// <param name="yMin">Minimum y value of the data points.</param>
        /// <param name="yMax">Maximum y value of the data points.</param>
        private static void MakeWorldFile(string imagePath, double xMin, double xMax, double yMin, double yMax, int xDistance, int yDistance)
        {
            string worldFilePath = imagePath.Remove(imagePath.Length - 3) + "pgw";
            //xMultiplier is a measure of how many pixels per meter. To find the center, we instead want to know how many meters there are in a pixel and divide that by 2 to find the middle.
            double xPixelCenter = xMin + (xMax - xMin) / xDistance / 2;
            double yPixelCenter = yMax - (yMax - yMin) / yDistance / 2;
            string output = (xMax - xMin) / xDistance + "\n" + "0" + "\n" + 0 + "\n" + (yMin - yMax) / yDistance + "\n" +
                            xPixelCenter + "\n" + yPixelCenter;
            File.WriteAllText(worldFilePath, output);
        }
        
        /// <summary>
        /// Convert the binary (.las/.laz) data lidar data file to ASCII that can be parsed.
        /// </summary>
        /// <param name="filePaths">List of lidar data (.las/.laz) paths.</param>
        /// <param name="genericFilePaths">List of lidar data paths without extensions.</param>
        private static void ConvertBinaryToXyzci(string lidarFilePath, string txtPath)
        {
            //Output is in X, Y, Z, C format
            //Check if on Linux, Windows or another OS, as that impact how to run the conversion tool.
            //Console.WriteLine("OS detected: " + System.Environment.OSVersion.Platform);
            if (Environment.OSVersion.Platform == PlatformID.Unix)
            { //If on Linux
                try
                { //Run the program from bin.
                    //if(printConsole) Console.WriteLine($"Converting {genericFilePaths[iteration]} to txt.");
                    ProcessStartInfo processStartInfo = new ProcessStartInfo("/bin/las2txt",
                        $"-i {lidarFilePath} -o {txtPath} --parse xyzci");
                    Process process = new Process();
                    process.StartInfo = processStartInfo;
                    process.Start();
                    process.WaitForExit();
                }
                catch (Exception e)
                { //If the program isn't installed (in bin).
                    Console.WriteLine("Are you missing las2txt? " + e);
                    Environment.Exit(1); //Quit
                }
            }
            else if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            { //If on Windows
                try
                { //Run the binary in the current folder.
                    Console.WriteLine("Converting binary to txt.");
                    ProcessStartInfo processStartInfo = new ProcessStartInfo("las2txt",
                        $"-i {lidarFilePath} -o {txtPath} -parse xyzci -sep comma");
                    Process process = new Process();
                    process.StartInfo = processStartInfo;
                    process.Start();
                    process.WaitForExit();
                }
                catch (Exception e)
                { //If the binary doesn't exist in the same folder.
                    Console.WriteLine("Are you missing las2txt?\nPlease put it in the same directory as LaserDataImage." +
                                      e);
                    Environment.Exit(1); //Quit
                }
            }
            else
            { //If on another OS
                Console.WriteLine("Your OS is currently not supported.");
                Environment.Exit(1); //Quit
            }
        }
        /// <summary>
        /// Returns a list of all lidar files in the specified directory.
        /// </summary>
        /// <param name="dir">Directory to search.</param>
        /// <returns>List of all lidar files in the specified directory.</returns>
        private static List<string> LidarFilesInDir(string dir)
        {
            List<string> lidarFiles = new List<string>();
            if (Directory.Exists(dir))
            {
                string[] files = Directory.GetFiles(dir);
                foreach (string fileName in files)
                {
                    if (Regex.IsMatch(fileName, "\\.las|\\.laz"))
                    { // \\. to treat the . like any other character, | means either
                        lidarFiles.Add(fileName);
                    }
                }
            }
            return lidarFiles;
        }
        /// <summary>
        /// Find the highest/lowest x, y and z values.
        /// </summary>
        /// <param name="data">Data to search through.</param>
        /// <param name="xMin">Minimum x-value.</param>
        /// <param name="yMin">Minimum y-value.</param>
        /// <param name="zMin">Minimum z-value.</param>
        /// <param name="xMax">Maximum x-value.</param>
        /// <param name="yMax">Maximum z-value.</param>
        /// <param name="zMax">Maximum z-value.</param>
        private static void FindMinMax(DataPoint[] data, out double xMin, out double yMin, out double zMin, out int iMin,
            out double xMax, out double yMax, out double zMax, out int iMax)
        {
            //Ensure each variable has a value (because their out).
            xMin = data[0].X;
            yMin = data[0].Y;
            zMin = data[0].Z;
            iMin = data[0].I;
            xMax = data[0].X;
            yMax = data[0].Y;
            zMax = data[0].Z;
            iMax = data[0].I;
            //Iterate through and find lowest/highest values
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i].X < xMin)
                    xMin = data[i].X;
                if (data[i].Y < yMin)
                    yMin = data[i].Y;
                if (data[i].Z < zMin)
                    zMin = data[i].Z;
                if (data[i].I < iMin)
                    iMin = data[i].I;
                if (data[i].X > xMax)
                    xMax = data[i].X;
                if (data[i].Y > yMax)
                    yMax = data[i].Y;
                if (data[i].Z > zMax)
                    zMax = data[i].Z;
                if (data[i].I > iMax)
                    iMax = data[i].I;
            }
            //Console.WriteLine($"xMin: {xMin}, xMax: {xMax}, yMin: {yMin}, yMax: {yMax}, zMin: {zMin}, zMax: {zMax}");
            Console.WriteLine($"North-eastern corner at: {xMin}, {yMax}");
        }
        
        private static void GetLowHigh(double[,] data, out double min, out double max)
        {
            //Ensure each variable has a value (because their out).
            min = data[0, 0];
            max = data[0, 0];
            //Iterate through and find lowest/highest values
            for (int i = 0; i < data.GetLength(0); i++)
            {
                for (int j = 0; j < data.GetLength(1); j++)
                {
                    if (data[i, j] < min)
                        min = data[i, j];
                    if (data[i, j] > max)
                        max = data[i, j];
                }
            }
        }
        
        /// <summary>
        /// Split the data points into their respective data classes.
        /// </summary>
        /// <param name="data">Input data containing all data classes.</param>
        /// <param name="uData">Returns unclassified data points.</param>
        /// <param name="gData">Returns ground data points.</param>
        /// <param name="lData">Returns low-noise data points.</param>
        /// <param name="wData">Returns water data points.</param>
        /// <param name="hData">Returns high-noise data points.</param>
        private static void SplitDataClasses(IEnumerable<DataPoint> data, out List<DataPoint> uData,
            out List<DataPoint> gData, out List<DataPoint> lData, out List<DataPoint> wData, out List<DataPoint> hData)
        {
            uData = new List<DataPoint>(); //1
            gData = new List<DataPoint>(); //2
            lData = new List<DataPoint>(); //7
            wData = new List<DataPoint>(); //9
            hData = new List<DataPoint>(); //18
            foreach (DataPoint t in data)
            {
                switch (t.C)
                {
                    case 1:
                        uData.Add(t);
                        break;
                    case 2:
                        gData.Add(t);
                        break;
                    case 7:
                        lData.Add(t);
                        break;
                    case 9:
                        wData.Add(t);
                        break;
                    case 18:
                        hData.Add(t);
                        break;
                }
            }
        }
        /// <summary>
        /// Color the image by class.
        /// </summary>
        /// <param name="data">Data to use.</param>
        /// <param name="image">Image to draw on.</param>
        /// <param name="xMultiplier">Data points/meter in x.</param>
        /// <param name="yMultiplier">Data points/meter in y.</param>
        private static void PixelClassColoring(DataPoint[] data, ref Bitmap image, double xMultiplier, double yMultiplier, SelectedArea selectedArea)
        {
            Console.WriteLine("x multiplier: " + xMultiplier);
            Console.WriteLine("y multiplier: " + yMultiplier);
            SplitDataClasses(data, out List<DataPoint> unclassifiedDataPoints, out List<DataPoint> groundDataPoints, out List<DataPoint> lowNoiseDataPoints,
                out List<DataPoint> groundWaterDataPoints, out List<DataPoint> highNoiseDataPoints);
            List<DataPoint>[] classifiedData =
            {
                unclassifiedDataPoints, groundDataPoints, lowNoiseDataPoints, groundWaterDataPoints, highNoiseDataPoints
            };
            //Declare array of colors for easy iteration
            Color[] colors =
            {
                Color.Black, Color.Yellow, Color.White, Color.Blue, Color.Green
            };
            //Loop through all the lists of data points and color their corresponding pixels.
            for (int i = 0; i < classifiedData.Length; i++)
            {
                for (int j = 0; j < classifiedData[i].Count; j++)
                {
                    image.SetPixel((int)(classifiedData[i][j].X % selectedArea.XDistance),
                        (int)(classifiedData[i][j].Y % selectedArea.YDistance), colors[i]);
                }
            }
        }
        /// <summary>
        /// Color based on percentage of range (difference between maximum and minimum elevation/z-value).
        /// </summary>
        /// <param name="data">Data points to process.</param>
        /// <param name="image">Image to draw on.</param>
        private static void PixelElevationColoring(DataPoint[] data, ref Bitmap image, string fileName, SelectedArea selectedArea)
        {
            SplitDataClasses(data, out _, out List<DataPoint> groundDataPoints, out _, out _, out _);
            double[,] elevationGrid = GetDataRaster(groundDataPoints, GridType.GroundElevation, fileName, selectedArea);
            GetLowHigh(elevationGrid, out double zMin, out double zMax);
            //Calculate the range for the elevation
            double zRange = zMax - zMin;
            Console.WriteLine($"Z range ground data: {zRange}");
            //Color the pixel corresponding to the DataPoint based on how high/low it is in the zRange (higher is brighter).
            for (int i = 0; i < elevationGrid.GetLength(0); i++)
            {
                for (int j = 0; j < elevationGrid.GetLength(1); j++)
                {
                    int value = (int)((elevationGrid[i, j] - zMin) / zRange * 255);
                    image.SetPixel(i, j, Color.FromArgb(value, value, value));
                }
            }
        }

        private static double[,] GaussianFilter(double[,] elevationGrid)
        {
            double[,] smoothedGrid = new double[elevationGrid.GetLength(0), elevationGrid.GetLength(1)];

            /*// A P P L Y   K E R N E L

            // a | b | c    1 | 2 | 1
            // d | e | f    2 | 4 | 2
            // g | h | i    1 | 2 | 1

            for (int x = 1; x < elevationGrid.GetLength(0) - 1; x++)
            {
                for (int y = 1; y < elevationGrid.GetLength(1) - 1; y++)
                {
                    double a = elevationGrid[x - 1, y - 1];
                    double b = elevationGrid[x, y - 1];
                    double c = elevationGrid[x + 1, y - 1];
                    double d = elevationGrid[x - 1, y];
                    double e = elevationGrid[x, y];
                    double f = elevationGrid[x + 1, y];
                    double g = elevationGrid[x - 1, y + 1];
                    double h = elevationGrid[x, y + 1];
                    double i = elevationGrid[x + 1, y + 1];
                    double value = (a + b * 2 + c + d * 2 + e * 4 + f * 2 + g + h * 2 + i) / 16;
                    smoothedGrid[x, y] = value;
                }
            }*/

            int radius = 2;
            int[] valueArray = new int[] {1,4,7,4,1,4,16,26,16,4,7,26,41,26,7,4,16,26,16,4,1,4,7,4,1};

            for (int x = radius; x < elevationGrid.GetLength(0) - radius; x++)
            {
                for (int y = radius; y < elevationGrid.GetLength(1) - radius; y++)
                {
                    double sum = 0;
                    int iterator = 0;
                    for (int xx = -radius; xx <= radius; xx++)
                    {
                        for (int yy = -radius; yy <= radius; yy++)
                        {
                            sum += elevationGrid[x + xx, y + yy] * valueArray[iterator];
                            ++iterator;
                        }
                    }
                    smoothedGrid[x, y] = sum / 273;
                }
            }

            // Fill not smoothed borders
            for (int x = 0; x < elevationGrid.GetLength(0); x++)
            {
                for (int y = 0; y < elevationGrid.GetLength(1); y++)
                {
                    if (x > 2 && x < elevationGrid.GetLength(0) - 2)
                        continue;
                    smoothedGrid[x, y] = elevationGrid[x, y];
                }
            }
            for (int y = 0; y < elevationGrid.GetLength(1); y++)
            {
                for (int x = 0; x < elevationGrid.GetLength(0); x++)
                {
                    if (y > 2 && y < elevationGrid.GetLength(1) - 2)
                        continue;
                    smoothedGrid[x, y] = elevationGrid[x, y];
                }
            }

            return smoothedGrid;
        }

        private static void PixelContourDrawing(DataPoint[] data, ref Bitmap image, string fileName, SelectedArea selectedArea, double contourHeight = 5)
        {
            SplitDataClasses(data, out _, out List<DataPoint> groundDataPoints, out _,
                out _, out _);
            double[,] elevationGrid = GetDataRaster(groundDataPoints, GridType.GroundElevation, fileName, selectedArea);
            elevationGrid = GaussianFilter(elevationGrid);
            //elevationGrid = GaussianFilter(elevationGrid);
            //elevationGrid = GaussianFilter(elevationGrid);
            //elevationGrid = GaussianFilter(elevationGrid);

            double maxHeight = elevationGrid[0, 0];
            double minHeight = elevationGrid[0, 0];
            foreach (double height in elevationGrid)
            {
                if (height > maxHeight)
                    maxHeight = height;
                if (height < minHeight)
                    minHeight = height;
            }
            minHeight -= 0.001F; //Avoid division with zero
            Console.WriteLine("Highest value: " + maxHeight);
            Console.WriteLine("Lowest value: " + minHeight);
            
            List<ContourLine> contours = new List<ContourLine>();
            
            //Create grid of lists containing polylines (lists of points)
            List<ContourLine>[,] contourGrid = new List<ContourLine>[selectedArea.XDistance, selectedArea.YDistance];
            for (int i = 0; i < contourGrid.GetLength(0); i++)
            {
                for (int j = 0; j < contourGrid.GetLength(1); j++)
                {
                    contourGrid[i,j] = new List<ContourLine>();
                }
            }
            //  a | b
            //  --+--
            //  d | c

            Console.WriteLine("Initialized variables");
            for (int x = 0; x < elevationGrid.GetLength(0) - 1; x++)
            {
                for (int y = 0; y < elevationGrid.GetLength(1) - 1; y++)
                {
                    if (x == elevationGrid.GetLength(0) - 2 && y == elevationGrid.GetLength(1) - 2)
                    {
                        break;
                    }

                    int[] x_additions = new int[] { 0, 1, 1, 0 };
                    int[] y_additions = new int[] { 0, 0, 1, 1 };
                    int[] location_multiplier = new int[] { 1, 1, -1, -1 };

                    double a = elevationGrid[x, y];
                    double b = elevationGrid[x + 1, y];
                    double c = elevationGrid[x + 1, y + 1];
                    double d = elevationGrid[x, y + 1];
                    if (double.IsNaN(a) || double.IsNaN(b) || double.IsNaN(c) || double.IsNaN(d))
                    {
                        //Console.WriteLine("Missing data. Unable to process.");savingContours.Add(contourLine);
                        continue;
                    }
                    double[] values = new double[] { a, b, c, d };
                    List<ContourPoint> points = new List<ContourPoint>();

                    for (int i = 0; i < 4; i++)
                    {
                        double smallest = 0;
                        double largest = 0;
                        if (values[i] > values[(i + 1) % 4])
                        {
                            largest = values[i];
                            smallest = values[(i + 1) % 4];
                        } else
                        {
                            largest = values[(i + 1) % 4];
                            smallest = values[i];
                        }
                        if ((int)(smallest/5) == (int)(largest/5))
                            continue;
                        double curve_height = ((int)(smallest / 5) + 1) * 5;
                        double part_of_way = curve_height - values[i];
                        double difference = values[(i + 1) % 4] - values[i];
                        double curve_location = part_of_way / difference;
                        double abs_x = x + x_additions[i];
                        double abs_y = y + y_additions[i];
                        if (i % 2 == 0)
                            abs_x += curve_location * location_multiplier[i];
                        else
                            abs_y += curve_location * location_multiplier[i];
                        image.SetPixel((int)x, (int)y, Color.FromArgb(200, 100, 0));
                        ContourPoint newCord = new ContourPoint(abs_x, abs_y, curve_height);
                        points.Add(newCord);
                    }
                    List<List<ContourPoint>> splitPoints = points.GroupBy(obj => obj.Z).Select(obj => obj.ToList()).ToList();
                    foreach (List<ContourPoint> item in splitPoints)
                    {
                        if (item.Count > 1)   //TODO:When is this not true?
                        {
                            if (item.Count == 4)
                            {
                                //Calculate center to find if it's high or low
                                double center = (a + b + c + d) / 4;

                                List<ContourPoint> pointsA = new List<ContourPoint>();
                                List<ContourPoint> pointsB = new List<ContourPoint>();
                                if (center > item[0].Z)
                                {   //High center, include in highs
                                    if (a > item[0].Z || c > item[0].Z)
                                    {
                                        pointsA = new List<ContourPoint> { item[0], item[1] };
                                        pointsB = new List<ContourPoint> { item[2], item[3] };
                                    } else if (b > item[0].Z || d > item[0].Z)
                                    {
                                        pointsA = new List<ContourPoint> { item[0], item[3] };
                                        pointsB = new List<ContourPoint> { item[1], item[2] };
                                    }
                                }
                                else
                                {   //Low center, include in lows
                                    if (a > item[0].Z || c > item[0].Z)
                                    {
                                        pointsA = new List<ContourPoint> { item[0], item[3] };
                                        pointsB = new List<ContourPoint> { item[1], item[2] };
                                    }
                                    if (b > item[0].Z || d > item[0].Z)
                                    {
                                        pointsA = new List<ContourPoint> { item[0], item[1] };
                                        pointsB = new List<ContourPoint> { item[2], item[3] };
                                    }
                                }
                                contours.Add(new ContourLine(pointsA[0], pointsA[1], x, y));
                                contours.Add(new ContourLine(pointsB[0], pointsB[1], x, y));
                                contourGrid[x, y].Add(new ContourLine(pointsA[0], pointsA[1], x, y));
                                contourGrid[x, y].Add(new ContourLine(pointsB[0], pointsB[1], x, y));
                                Console.WriteLine("Ambiguity processed!");
                            }
                            else
                            {
                                contours.Add(new ContourLine(item[0], item[1], x, y));
                                contourGrid[x, y].Add(new ContourLine(item[1], item[0], x, y)); //Important that it's in order [1], then [0], so that it follows left to right
                            }
                        }
                    }
                }
            }

            Console.WriteLine("Edges found");
            Console.WriteLine("old contours count: " + contours.Count);

            //Save raw contours
            DxfDocument rawDxfOutput = new DxfDocument();
            foreach (ContourLine contourLine in contours)
            {
                List<Vector2> lines = new List<Vector2>();
                foreach (ContourPoint contourPoint in contourLine.ContourPoints)
                {
                    lines.Add(new Vector2(contourPoint.X, contourPoint.Y));
                }
                LwPolyline polyline = new LwPolyline(lines);
                rawDxfOutput.AddEntity(polyline);
            }
            rawDxfOutput.Save(fileName.Remove(fileName.Length - 4) + "RawContours.dxf");
            Console.WriteLine("Raw contours saved");

            int connectedOnXAxis = 0;
            int connectedOnYAxis = 0;
            int ignoredOnXAxis = 0;
            int ignoredOnYAxis = 0;
            //Save collected contours
            for (int x = 1; x < contourGrid.GetLength(0); x++)
            {
                for (int y = 1; y < contourGrid.GetLength(1); y++)
                {
                    for (int j = 0; j < contourGrid[x, y].Count; j++)
                    {
                        for (int i = 0; i < contourGrid[x - 1, y].Count; i++)
                        {
                            if (contourGrid[x - 1, y][i].Equals(contourGrid[x, y][j]))  //Circle
                                continue;
                            if (contourGrid[x - 1, y][i].GetLast().Equals(contourGrid[x, y][j].GetFirst()))
                            {
                                //Make into one line; remove from original
                                ContourLine contourLine1 = contourGrid[x - 1, y][i]; //Save first contour line
                                ContourLine contourLine2 = contourGrid[x, y][j];    //Save second contour line
                                ContourLine contourLineNew = contourLine1;
                                if (contourLine1.Count() != 2)
                                    contourGrid[contourLine1.LastEnd[0], contourLine1.LastEnd[1]].Remove(contourLine1);
                                if (contourLine2.Count() != 2)
                                    contourGrid[contourLine2.FirstEnd[0], contourLine2.FirstEnd[1]].Remove(contourLine2);
                                contourLine2.RemoveFirst();
                                contourLineNew.Append(contourLine2);
                                int firstIndex = contourGrid[contourLineNew.FirstEnd[0], contourLineNew.FirstEnd[1]]
                                    .IndexOf(contourLine1);
                                int secondIndex = contourGrid[contourLineNew.LastEnd[0], contourLineNew.LastEnd[1]]
                                    .IndexOf(contourLine2);
                                contourGrid[contourLineNew.FirstEnd[0], contourLineNew.FirstEnd[1]][firstIndex] = contourLineNew;
                                contourGrid[contourLineNew.LastEnd[0], contourLineNew.LastEnd[1]][secondIndex] = contourLineNew;
                                
                                ++connectedOnXAxis;
                                break;
                            }
                            if (contourGrid[x - 1, y][i].GetFirst().Equals(contourGrid[x, y][j].GetFirst()))
                            { 
                                ContourLine contourLine1 = contourGrid[x - 1, y][i]; //Save first contour line
                                ContourLine contourLine2 = contourGrid[x, y][j];    //Save second contour line
                                ContourLine contourLineNew = contourLine1;
                                if (contourLine1.Count() != 2)
                                    contourGrid[contourLine1.FirstEnd[0], contourLine1.FirstEnd[1]].Remove(contourLine1);
                                if (contourLine2.Count() != 2)
                                    contourGrid[contourLine2.FirstEnd[0], contourLine2.FirstEnd[1]].Remove(contourLine2);
                                contourLineNew.RemoveFirst();
                                contourLineNew.Reverse();
                                contourLineNew.Append(contourLine2);
                                int firstIndex = contourGrid[contourLineNew.FirstEnd[0], contourLineNew.FirstEnd[1]]
                                    .IndexOf(contourLine1);
                                int secondIndex = contourGrid[contourLineNew.LastEnd[0], contourLineNew.LastEnd[1]]
                                    .IndexOf(contourLine2);
                                contourGrid[contourLineNew.FirstEnd[0], contourLineNew.FirstEnd[1]][firstIndex] = contourLineNew;
                                contourGrid[contourLineNew.LastEnd[0], contourLineNew.LastEnd[1]][secondIndex] = contourLineNew;
                                
                                ++connectedOnXAxis;
                                break;
                            }
                            // Breaks when this is on
                            if (contourGrid[x - 1, y][i].GetLast().Equals(contourGrid[x, y][j].GetLast()))
                            {
                                ContourLine contourLine1 = contourGrid[x - 1, y][i]; //Save first contour line
                                ContourLine contourLine2 = contourGrid[x, y][j];    //Save second contour line
                                ContourLine contourLineNew = contourLine1;
                                if (contourLine1.Count() != 2)
                                    contourGrid[contourLine1.LastEnd[0], contourLine1.LastEnd[1]].Remove(contourLine1);
                                if (contourLine2.Count() != 2)
                                    contourGrid[contourLine2.LastEnd[0], contourLine2.LastEnd[1]].Remove(contourLine2);
                                contourLine2.RemoveLast();
                                contourLine2.Reverse();
                                contourLineNew.Append(contourLine2);
                                int firstIndex = contourGrid[contourLineNew.FirstEnd[0], contourLineNew.FirstEnd[1]]
                                    .IndexOf(contourLine1);
                                int secondIndex = contourGrid[contourLineNew.LastEnd[0], contourLineNew.LastEnd[1]]
                                    .IndexOf(contourLine2);
                                contourGrid[contourLineNew.FirstEnd[0], contourLineNew.FirstEnd[1]][firstIndex] = contourLineNew;
                                contourGrid[contourLineNew.LastEnd[0], contourLineNew.LastEnd[1]][secondIndex] = contourLineNew;
                                
                                ++connectedOnXAxis;
                                break;
                            }
                            if (contourGrid[x - 1, y][i].GetFirst().Equals(contourGrid[x, y][j].GetLast()))
                            {
                                ContourLine contourLine1 = contourGrid[x - 1, y][i]; //Save first contour line
                                ContourLine contourLine2 = contourGrid[x, y][j];    //Save second contour line
                                ContourLine contourLineNew = contourLine1;
                                if (contourLine1.Count() != 2)
                                    contourGrid[contourLine1.FirstEnd[0], contourLine1.FirstEnd[1]].Remove(contourLine1);
                                if (contourLine2.Count() != 2)
                                    contourGrid[contourLine2.LastEnd[0], contourLine2.LastEnd[1]].Remove(contourLine2);
                                contourLine2.RemoveLast();
                                contourLineNew.Reverse();
                                contourLine2.Reverse();
                                contourLineNew.Append(contourLine2);
                                int firstIndex = contourGrid[contourLineNew.FirstEnd[0], contourLineNew.FirstEnd[1]]
                                    .IndexOf(contourLine1);
                                int secondIndex = contourGrid[contourLineNew.LastEnd[0], contourLineNew.LastEnd[1]]
                                    .IndexOf(contourLine2);
                                contourGrid[contourLineNew.FirstEnd[0], contourLineNew.FirstEnd[1]][firstIndex] = contourLineNew;
                                contourGrid[contourLineNew.LastEnd[0], contourLineNew.LastEnd[1]][secondIndex] = contourLineNew;
                                    
                                ++connectedOnXAxis;
                                break;
                            }
                        }
                        if (contourGrid[x, y].Count == 0)   //Circle
                            continue;
                        for (int i = 0; i < contourGrid[x, y - 1].Count; i++)
                        {
                            if (contourGrid[x, y - 1][i].Equals(contourGrid[x, y][j]))
                                continue;
                            if (contourGrid[x, y - 1][i].GetFirst().Equals(contourGrid[x, y][j].GetLast()))
                            {
                                ContourLine contourLine1 = contourGrid[x, y - 1][i]; //Save first contour line
                                ContourLine contourLine2 = contourGrid[x, y][j];    //Save second contour line
                                ContourLine contourLineNew = contourLine1;
                                if (contourLine1.Count() != 2)
                                    contourGrid[contourLine1.FirstEnd[0], contourLine1.FirstEnd[1]].Remove(contourLine1);
                                if (contourLine2.Count() != 2)
                                    contourGrid[contourLine2.LastEnd[0], contourLine2.LastEnd[1]].Remove(contourLine2);
                                contourLine2.RemoveLast();
                                contourLineNew.Reverse();
                                contourLine2.Reverse();
                                contourLineNew.Append(contourLine2);
                                int firstIndex = contourGrid[contourLineNew.FirstEnd[0], contourLineNew.FirstEnd[1]]
                                    .IndexOf(contourLine1);
                                int secondIndex = contourGrid[contourLineNew.LastEnd[0], contourLineNew.LastEnd[1]]
                                    .IndexOf(contourLine2);
                                contourGrid[contourLineNew.FirstEnd[0], contourLineNew.FirstEnd[1]][firstIndex] = contourLineNew;
                                contourGrid[contourLineNew.LastEnd[0], contourLineNew.LastEnd[1]][secondIndex] = contourLineNew;

                                ++connectedOnYAxis;
                                break;
                            }
                            if (contourGrid[x, y - 1][i].GetLast().Equals(contourGrid[x, y][j].GetLast()))
                            {
                                ContourLine contourLine1 = contourGrid[x, y - 1][i]; //Save first contour line
                                ContourLine contourLine2 = contourGrid[x, y][j];    //Save second contour line
                                ContourLine contourLineNew = contourLine1;
                                if (contourLine1.Count() != 2)
                                    contourGrid[contourLine1.LastEnd[0], contourLine1.LastEnd[1]].Remove(contourLine1);
                                if (contourLine2.Count() != 2)
                                    contourGrid[contourLine2.LastEnd[0], contourLine2.LastEnd[1]].Remove(contourLine2);
                                contourLine2.RemoveLast();
                                contourLine2.Reverse();
                                contourLineNew.Append(contourLine2);
                                int firstIndex = contourGrid[contourLineNew.FirstEnd[0], contourLineNew.FirstEnd[1]]
                                    .IndexOf(contourLine1);
                                int secondIndex = contourGrid[contourLineNew.LastEnd[0], contourLineNew.LastEnd[1]]
                                    .IndexOf(contourLine2);
                                contourGrid[contourLineNew.FirstEnd[0], contourLineNew.FirstEnd[1]][firstIndex] = contourLineNew;
                                contourGrid[contourLineNew.LastEnd[0], contourLineNew.LastEnd[1]][secondIndex] = contourLineNew;
                                
                                ++connectedOnYAxis;
                                break;
                            }
                            if (contourGrid[x, y - 1][i].GetFirst().Equals(contourGrid[x, y][j].GetFirst()))
                            {
                                ContourLine contourLine1 = contourGrid[x, y - 1][i]; //Save first contour line
                                ContourLine contourLine2 = contourGrid[x, y][j];    //Save second contour line
                                ContourLine contourLineNew = contourLine1;
                                if (contourLine1.Count() != 2)
                                    contourGrid[contourLine1.FirstEnd[0], contourLine1.FirstEnd[1]].Remove(contourLine1);
                                if (contourLine2.Count() != 2)
                                    contourGrid[contourLine2.FirstEnd[0], contourLine2.FirstEnd[1]].Remove(contourLine2);
                                contourLineNew.RemoveFirst();
                                contourLineNew.Reverse();
                                contourLineNew.Append(contourLine2);
                                int firstIndex = contourGrid[contourLineNew.FirstEnd[0], contourLineNew.FirstEnd[1]]
                                    .IndexOf(contourLine1);
                                int secondIndex = contourGrid[contourLineNew.LastEnd[0], contourLineNew.LastEnd[1]]
                                    .IndexOf(contourLine2);
                                contourGrid[contourLineNew.FirstEnd[0], contourLineNew.FirstEnd[1]][firstIndex] = contourLineNew;
                                contourGrid[contourLineNew.LastEnd[0], contourLineNew.LastEnd[1]][secondIndex] = contourLineNew;
                                
                                ++connectedOnXAxis;
                                break;
                            }
                            if (contourGrid[x, y - 1][i].GetLast().Equals(contourGrid[x, y][j].GetFirst()))
                            {
                                //Make into one line; remove from original
                                ContourLine contourLine1 = contourGrid[x, y - 1][i]; //Save first contour line
                                ContourLine contourLine2 = contourGrid[x, y][j];    //Save second contour line
                                ContourLine contourLineNew = contourLine1;
                                if (contourLine1.Count() != 2)
                                    contourGrid[contourLine1.LastEnd[0], contourLine1.LastEnd[1]].Remove(contourLine1);
                                if (contourLine2.Count() != 2)
                                    contourGrid[contourLine2.FirstEnd[0], contourLine2.FirstEnd[1]].Remove(contourLine2);
                                contourLine2.RemoveFirst();
                                contourLineNew.Append(contourLine2);
                                int firstIndex = contourGrid[contourLineNew.FirstEnd[0], contourLineNew.FirstEnd[1]]
                                    .IndexOf(contourLine1);
                                int secondIndex = contourGrid[contourLineNew.LastEnd[0], contourLineNew.LastEnd[1]]
                                    .IndexOf(contourLine2);
                                contourGrid[contourLineNew.FirstEnd[0], contourLineNew.FirstEnd[1]][firstIndex] = contourLineNew;
                                contourGrid[contourLineNew.LastEnd[0], contourLineNew.LastEnd[1]][secondIndex] = contourLineNew;
                                
                                ++connectedOnXAxis;
                                break;
                            }
                        }
                    }
                }
            }
            
            Console.WriteLine("Lines connected on x-axis: " + connectedOnXAxis);
            Console.WriteLine("Lines connected on y-axis: " + connectedOnYAxis);
            Console.WriteLine("Lines ignored on x-axis: " + ignoredOnXAxis);
            Console.WriteLine("Lines ignored on y-axis: " + ignoredOnYAxis);
            int gridContourCount = 0;
            
            List<ContourLine> savingContours = new List<ContourLine>();
            foreach (List<ContourLine> list in contourGrid)
            {
                foreach (ContourLine contourLine in list)
                {
                    ++gridContourCount;
                    if(!savingContours.Contains(contourLine))
                        savingContours.Add(contourLine);
                }
            }
            Console.WriteLine("Contours after merging: " + gridContourCount);
            Console.WriteLine("Contours after removing duplicates: " + savingContours.Count);
            
            //Save collected contours
            DxfDocument collectedDxfOutput = new DxfDocument();
            DxfDocument splineDxf = new DxfDocument();
            double xMin = data[0].X;
            double yMin = data[0].Y;
            foreach (ContourLine contourLine in savingContours)
            {
                List<Vector2> lines = new List<Vector2>();
                List<SplineVertex> controlPoints = new List<SplineVertex>();
                int count = 0;
                foreach (ContourPoint contourPoint in contourLine.ContourPoints)
                {
                    lines.Add(new Vector2(xMin + contourPoint.X, yMin + contourPoint.Y));
                    if (count % 8 == 0)
                        controlPoints.Add(new SplineVertex(new Vector2(xMin + contourPoint.X, yMin + contourPoint.Y)));
                    count++;
                }
                LwPolyline polyline = new LwPolyline(lines);
                collectedDxfOutput.AddEntity(polyline);
                if (controlPoints.Count > 10)
                {
                    Spline spline = new Spline(controlPoints, 10);
                    splineDxf.AddEntity(spline);
                }
            }
            collectedDxfOutput.Save(fileName.Remove(fileName.Length - 4) + "CollectedContours.dxf");    //TODO: Should be added to the output folder instead
            Console.WriteLine("Collected contours saved");
            splineDxf.Save(fileName.Remove(fileName.Length - 4) + "Splines.dxf");
            Console.WriteLine("Splines saved");
        }

        private static void PixelVegetationElevationColoring(DataPoint[] dataPoints, ref Bitmap image, string fileName, SelectedArea selectedArea)
        {
            SplitDataClasses(dataPoints, out List<DataPoint> unclassifiedDataPoints, out List<DataPoint> groundDataPoints, out _,
                out _, out _);
            //Find highest and lowest values for ground data.
            //FindMinMax(groundDataPoints.ToArray(), out double _, out double _, out double zMin, out int _, out double _, out double _, out double zMax,
            //    out int _);
            double[,] elevationGrid = GetDataRaster(groundDataPoints, GridType.GroundElevation, fileName, selectedArea);
            double[,] vegetationElevationGrid =
                GetDataRaster(unclassifiedDataPoints, GridType.VegetationElevation, fileName, selectedArea);
            double[,] differenceGrid = new double[selectedArea.XDistance, selectedArea.YDistance];
            int belowGround = 0;
            for (int i = 0; i < differenceGrid.GetLength(0); i++)
            {
                for (int j = 0; j < differenceGrid.GetLength(1); j++)
                {
                    differenceGrid[i, j] = Math.Abs(vegetationElevationGrid[i, j] - elevationGrid[i, j]);
                    //if (differenceGrid[i, j] < 0)
                    //    belowGround++;
                }
            }
            GetLowHigh(differenceGrid, out double diffMin, out double diffMax);
            double diffRange = diffMax - diffMin;
            //Console.WriteLine($"The vegetation was below the ground {belowGround} times.");
            for (int i = 0; i < selectedArea.XDistance; i++)
            {
                for (int j = 0; j < selectedArea.YDistance; j++)
                {
                    int value = (int)((differenceGrid[i, j] - diffMin) / diffRange * 255);
                    image.SetPixel(i, j, Color.FromArgb(value, value, value));
                }
            }
        }
        
        /// <summary>
        /// Transparent background with water, then unclassified data (like buildings), then the ground colors based on their elevation.
        /// </summary>
        /// <param name="data">Data points to process.</param>
        /// <param name="image">Image to draw on.</param>
        private static void PixelUnifiedElevationColoring(DataPoint[] data, ref Bitmap image, string fileName, SelectedArea selectedArea)
        {
            SplitDataClasses(data, out List<DataPoint> unclassifiedDataPoints, out List<DataPoint> groundDataPoints, out List<DataPoint> _,
                out List<DataPoint> groundWaterDataPoints, out List<DataPoint> _);
            //Add ground color gradient, like in pixelElevationColoring.
            double[,] elevationGrid = GetDataRaster(groundDataPoints, GridType.GroundElevation, fileName, selectedArea);
            GetLowHigh(elevationGrid, out double zMin, out double zMax);
            double zRange = zMax - zMin;
            Console.WriteLine($"Z range ground data: {zRange}");
            //Color the pixel corresponding to the DataPoint based on how high/low it is in the zRange (higher is brighter).
            for (int i = 0; i < elevationGrid.GetLength(0); i++)
            {
                for (int j = 0; j < elevationGrid.GetLength(1); j++)
                {
                    int value = (int)((elevationGrid[i, j] - zMin) / zRange * 255);
                    image.SetPixel(i, j, Color.FromArgb(value, value, 0));
                }
            }
            //Add buildings
            foreach (DataPoint t in unclassifiedDataPoints)
            {
                image.SetPixel((int)(t.X % selectedArea.XDistance),
                    (int)(t.Y % selectedArea.YDistance), Color.Black);
            }
            //Add water
            foreach (DataPoint d in groundWaterDataPoints)
            {
                image.SetPixel((int)(d.X % selectedArea.XDistance), (int)(d.Y % selectedArea.YDistance), Color.Blue);
            }
        }
        /// <summary>
        /// Color based on elevation layers
        /// </summary>
        /// <param name="data">Data points to process.</param>
        /// <param name="image">Image to draw on.</param>
        private static void PixelLayerColoring(DataPoint[] data, ref Bitmap image, string fileName, SelectedArea selectedArea, int layerHeight = 10)
        {
            SplitDataClasses(data, out _, out List<DataPoint> groundDataPoints, out _,
                out _, out _);
            double[,] elevationGrid = GetDataRaster(groundDataPoints, GridType.GroundElevation, fileName, selectedArea);
            //Calculate the range for the elevation
            GetLowHigh(elevationGrid, out double zMin, out double zMax);
            double zRange = zMax - zMin;
            Console.WriteLine($"Z range ground data: {zRange}");
            //Color the pixel corresponding to the DataPoint based on how high/low it is in the zRange (higher is brighter).
            for (int i = 0; i < elevationGrid.GetLength(0); i++)
            {
                for (int j = 0; j < elevationGrid.GetLength(1); j++)
                {
                    int value = (int)((elevationGrid[i, j] - zMin) / zRange * 255 / layerHeight) * layerHeight;
                    image.SetPixel(i, j, Color.FromArgb(value, value, value));
                }
            }
        }
        /// <summary>
        /// Color based on intensity data.
        /// </summary>
        /// <param name="data">Data points to porcess.</param>
        /// <param name="image">Image to draw on.</param>
        private static void PixelIntensityColoring(DataPoint[] data, ref Bitmap image, string fileName, SelectedArea selectedArea)
        {
            double[,] intensityGrid = GetDataRaster(data, GridType.Intensity, fileName, selectedArea);
            //Find highest and lowest values for ground data.
            GetLowHigh(intensityGrid, out double iMin, out double iMax);
            //Calculate the range for the elevation
            double iRange = iMax - iMin;
            Console.WriteLine($"Intensity range ground data: {iRange}");
            //Color the pixel corresponding to the DataPoint based on how high/low it is in the zRange (higher is brighter).
            for (int i = 0; i < intensityGrid.GetLength(0); i++)
            {
                for (int j = 0; j < intensityGrid.GetLength(1); j++)
                {
                    //int value = (int)((intensityGrid[i, j] - iMin) / iRange * 255);
                    int value = (int)(255 - 255/(intensityGrid[i, j]/10000 + 1));
                    image.SetPixel(i, j, Color.FromArgb(value, value, value));
                }
            }
        }

        private static double[,] GetDataRaster(IEnumerable<DataPoint> dataPoints, GridType gridType, string fileName, 
            SelectedArea selectedArea, bool forceNewData=false)
        {   
            string fileNameWithoutExtension = fileName.Remove(fileName.Length - 4);
            if (!forceNewData && gridType == GridType.GroundElevation && File.Exists(fileNameWithoutExtension + "GroundElevation.txt"))
            {
                Console.WriteLine("Reading elevation grid from file.");
                string textData = File.ReadAllText(fileNameWithoutExtension + "GroundElevation.txt");
                return JsonConvert.DeserializeObject<double[,]>(textData);
            }
            if (!forceNewData && gridType == GridType.Intensity && File.Exists(fileNameWithoutExtension + "Intensity.txt"))
            {
                Console.WriteLine("Reading intensity grid from file.");
                string textData = File.ReadAllText(fileNameWithoutExtension + "Intensity.txt");
                return JsonConvert.DeserializeObject<double[,]>(textData);
            }
            if (!forceNewData && gridType == GridType.VegetationElevation &&
                File.Exists(fileNameWithoutExtension + "VegetationElevation.txt"))
            {
                Console.WriteLine("Reading vegetation elevation grid from file.");
                string textData = File.ReadAllText(fileNameWithoutExtension + "VegetationElevation.txt");
                return JsonConvert.DeserializeObject<double[,]>(textData);
            }
            if (!forceNewData && gridType == GridType.unidentifiedElevation &&
                File.Exists(fileNameWithoutExtension + "UnidentifiedElevation.txt"))
            {
                Console.WriteLine("Reading unidentified class elevation grid from file.");
                string textData = File.ReadAllText(fileNameWithoutExtension + "UnidentifiedElevation.txt");
                return JsonConvert.DeserializeObject<double[,]>(textData);
            }
            Console.WriteLine("Creating elevation grid.");
            Console.WriteLine("Interpolating data...");
            double[,] elevationGrid = new double[selectedArea.XDistance, selectedArea.YDistance];
            List<DataPoint>[,] pointsInGrid = new List<DataPoint>[selectedArea.DataXDistance, selectedArea.DataYDistance];
            //Instantiate array of lists
            for (int x = 0; x < pointsInGrid.GetLength(0); x++)
                for (int y = 0; y < pointsInGrid.GetLength(1); y++)
                    pointsInGrid[x, y] = new List<DataPoint>();

            // Put points in list raster
            foreach (DataPoint d in dataPoints)
            {
                int gridX = (int)(d.X % selectedArea.DataXDistance);
                int gridY = (int)(d.Y % selectedArea.DataYDistance);
                pointsInGrid[gridX, gridY].Add(d);
            }

            // Select pixels from raster
            List<DataPoint>[,] selectedPoints = new List<DataPoint>[selectedArea.XDistance, selectedArea.YDistance];
            for (int i = selectedArea.MinX; i < selectedArea.MaxX; i++)
            {
                for (int j = selectedArea.MinY; j < selectedArea.MaxY; j++)
                {
                    selectedPoints[i - selectedArea.MinX, j - selectedArea.MinY] = pointsInGrid[i, j];
                }
            }
            int pointWidth = selectedPoints.GetLength(0);   //TODO: Check that these are the right dimensions (x/y)
            int pointHeight = selectedPoints.GetLength(1);

            int perfectCentres = 0;
            //Console.WriteLine("Separated data into grid.");
            for (int i = 0; i < pointWidth; i++)
            {
                for (int j = 0; j < pointHeight; j++)
                {
                    //Find 10 closest points (approximate)
                    DataPoint[] pointsWithDistance = new DataPoint[100];
                    int index = 0;
                    int n = 0;
                    while (index < 100) //TODO: Add max to n while avoiding null exceptions
                    {
                        int y = j - n;
                        int x;
                        //Process center square
                        if (n == 0)
                        {
                            foreach (DataPoint d in selectedPoints[i, j])
                            {
                                //If there is a point at the exact location, use that value directly
                                if (i - d.X % pointWidth + j - d.Y % pointHeight == 0)
                                {
                                    ++perfectCentres;
                                    if (gridType is GridType.GroundElevation or GridType.VegetationElevation)
                                        elevationGrid[i, j] = d.Z;
                                    else if (gridType == GridType.Intensity)
                                        elevationGrid[i, j] = d.I;
                                    break;
                                }
                            }
                            ++n;
                            continue;
                        }
                        
                        //TODO: Both first and second for loops are run for n=0, giving double values (probably more at times)
                        for (x = i - n; x <= i + n; x++)        
                        {
                            if (x < 0 || x >= pointWidth || y < 0 || y >= pointHeight)
                            {
                                continue;
                            }
                            foreach (DataPoint d in selectedPoints[x, y])
                            {
                                if (index == 100) 
                                {
                                    break;
                                }
                                d.Distance = (double)(Math.Pow(i - (d.X % pointWidth), 2) + Math.Pow(j - (d.Y % pointHeight), 2));
                                //Console.WriteLine("Distance: " + d.Distance);
                                //Console.WriteLine("Longest distance: " + closestTen[closestTen.Length - 1].Distance);
                                pointsWithDistance[index] = d;
                                index++;
                            }
                        }
                        y = j + n;
                        for (x = i - n; x <= i + n; x++)
                        {
                            if (x < 0 || x >= pointWidth || y < 0 || y >= pointHeight)
                            {
                                continue;
                            }
                            foreach (DataPoint d in selectedPoints[x, y])
                            {
                                if (index == 100)
                                {
                                    break;
                                }
                                d.Distance = (Math.Pow(i - (d.X % pointWidth), 2) + Math.Pow(j - (d.Y % pointHeight), 2));
                                //Console.WriteLine("Distance: " + d.Distance);
                                //Console.WriteLine("Longest distance: " + closestTen[closestTen.Length - 1].Distance);
                                pointsWithDistance[index] = d;
                                index++;
                            }
                        }
                        x = i - n;
                        for (y = j - n + 1; y <= j + n - 1; y++)
                        {
                            if (x < 0 || x >= pointWidth || y < 0 || y >= pointHeight)
                            {
                                continue;
                            }
                            foreach (DataPoint d in selectedPoints[x, y])
                            {
                                if (index == 100)
                                {
                                    break;
                                }
                                d.Distance = (double)(Math.Pow(i - (d.X % pointWidth), 2) + Math.Pow(j - (d.Y % pointHeight), 2));
                                //Console.WriteLine("Distance: " + d.Distance);
                                //Console.WriteLine("Longest distance: " + closestTen[closestTen.Length - 1].Distance);
                                pointsWithDistance[index] = d;
                                index++;
                            }
                        }
                        x = i + n;
                        for (y = j - n + 1; y <= j + n - 1; y++)
                        {
                            if (x < 0 || x >= pointWidth || y < 0 || y >= pointHeight)
                            {
                                continue;
                            }
                            foreach (DataPoint d in selectedPoints[x, y])
                            {
                                if (index == 100)
                                {
                                    break;
                                }
                                d.Distance = (double)(Math.Pow(i - (d.X % pointWidth), 2) + Math.Pow(j - (d.Y % pointHeight), 2));
                                //Console.WriteLine("Distance: " + d.Distance);
                                //Console.WriteLine("Longest distance: " + closestTen[closestTen.Length - 1].Distance);
                                pointsWithDistance[index] = d;
                                index++;
                            }
                        }
                        ++n;
                    }
                    Array.Sort(pointsWithDistance, DataPoint.SortDistance());
                    double numerator = 0; //Top of equation
                    double denominator = 0; //Bottom of equation
                    for (int k = 0; k < 10; k++)
                    {
                        pointsWithDistance[k].Distance = (double)(Math.Sqrt(Math.Pow(i - (pointsWithDistance[k].X % pointWidth), 2) +
                                                                   Math.Pow(j - (pointsWithDistance[k].Y % pointHeight), 2)));
                        //Console.WriteLine("One of ten closest: " + d.X + "," + d.Y + ":" + d.Distance);
                        if(gridType is GridType.GroundElevation or GridType.VegetationElevation or GridType.unidentifiedElevation)
                            numerator += pointsWithDistance[k].Z / pointsWithDistance[k].Distance;
                        else if (gridType == GridType.Intensity)
                            numerator += pointsWithDistance[k].I / pointsWithDistance[k].Distance;
                        denominator += 1 / pointsWithDistance[k].Distance; //d.Distance can be squared for another result.
                    }
                    //TODO: Why are there identical data points in pointsWithDistance?
                    double z = numerator / denominator;
                    if (double.IsNaN(z) || double.IsNaN(denominator) || double.IsNaN(numerator) || double.IsInfinity(z))
                    {
                        //++perfectCentres;
                        Console.WriteLine("We've got quite an issue here!");
                    }
                    elevationGrid[i, j] = z;
                    
                }
            }
            Console.WriteLine("Perfect centres: " + perfectCentres);
            Console.WriteLine("Added data to elevation grid.");
            //Save elevation grid (for future processing)
            if (selectedArea.IsFullSize())
            {
                SaveElevationGrid(elevationGrid, fileName, gridType);
            }
            return elevationGrid;
        }

        private static void SaveElevationGrid(double[,] elevationGrid, string fileName, GridType gridType)
        {
            Console.WriteLine("Storing for future use...\nSerializing Json.");
            string jsonString = JsonConvert.SerializeObject(elevationGrid);
            DirectoryInfo parentDirectory = Directory.GetParent(fileName);
            string tmpDirectory = parentDirectory + "/tmp/";
            if (!Directory.Exists(tmpDirectory))
            {
                Directory.CreateDirectory(tmpDirectory);
            }
            Console.Write("Writing data to file.");
            string gridPath = tmpDirectory + Path.GetFileNameWithoutExtension(fileName);
            if (gridType == GridType.GroundElevation)
                gridPath += "GroundElevation.txt";
            else if (gridType == GridType.Intensity)
                gridPath += "Intensity.txt";
            else if (gridType == GridType.VegetationElevation)
                gridPath += "VegetationElevation.txt";
            else if (gridType == GridType.unidentifiedElevation)
                gridPath += "UnidentifiedElevation.txt";
            File.WriteAllText(gridPath, jsonString);
            Console.WriteLine("Done!");
        }

        private static double[,] GetSlopeGrid(IEnumerable<DataPoint> dataPoints, string fileName, SelectedArea selectedArea, SlopeDirection slopeDirection, GridType gridType)
        {
            double[,] elevationGrid = GetDataRaster(dataPoints, gridType, fileName, selectedArea);
            //Find highest and lowest values for ground data.
            GetLowHigh(elevationGrid, out double zMin, out double zMax);
            //Calculate the range for the elevation
            double zRange = zMax - zMin;
            Console.WriteLine($"Z range ground data: {zRange}");
            //Populate slope grid
            double[,] slopeGrid = new double[elevationGrid.GetLength(0),elevationGrid.GetLength(1)];
            double highest = 0;
            double lowest = 1000;
            if (slopeDirection == SlopeDirection.Sum)
            {
                for (int i = 1; i < selectedArea.XDistance; i++)
                {
                    for (int j = 1; j < selectedArea.YDistance; j++)
                    {
                        double diff = Math.Pow(elevationGrid[i, j] - elevationGrid[i, j - 1], 2);
                        diff += Math.Pow(elevationGrid[i, j] - elevationGrid[i - 1, j], 2);
                        diff = Math.Sqrt(diff);
                        slopeGrid[i, j] = diff;
                        if (diff > highest)
                            highest = diff;
                        if (diff < lowest)
                            lowest = diff;
                    }
                }
            } else if (slopeDirection == SlopeDirection.Left)
            {
                for (int i = 1; i < elevationGrid.GetLength(0); i++)
                {
                    for (int j = 0; j < elevationGrid.GetLength(1); j++)
                    {
                        double diff = Math.Abs(elevationGrid[i, j] - elevationGrid[i - 1, j]);
                        slopeGrid[i, j] = diff;
                        if (diff > highest)
                            highest = diff;
                        if (diff < lowest)
                            lowest = diff;
                    }
                }
            } 
            else 
            {
                for (int i = 0; i < elevationGrid.GetLength(0); i++)
                {
                    for (int j = 1; j < elevationGrid.GetLength(1); j++)
                    {
                        double diff = Math.Abs(elevationGrid[i, j] - elevationGrid[i, j - 1]);
                        slopeGrid[i, j] = diff;
                        if (diff > highest)
                            highest = diff;
                        if (diff < lowest)
                            lowest = diff;
                    }
                }
            }
            Console.WriteLine($"Highest/lowest slope: {highest}/{lowest}");
            return slopeGrid;
        }
        
        private static void PixelSlopeElevationColoring(DataPoint[] dataPoints, ref Bitmap image, string fileName, SelectedArea selectedArea, SlopeDirection slopeDirection)
        {
            SplitDataClasses(dataPoints, out _, out List<DataPoint> groundDataPoints, out _,
                out _, out _);
            double[,] slopeGrid = GetSlopeGrid(groundDataPoints, fileName, selectedArea, slopeDirection, GridType.GroundElevation);
            GetLowHigh(slopeGrid, out double slopeMin, out double slopeMax);
            double slopeRange = slopeMax - slopeMin;
            for (int i = 0; i < slopeGrid.GetLength(0); i++)
            {
                for (int j = 1; j < slopeGrid.GetLength(1); j++)
                {
                    slopeGrid[i, j] = (int)((slopeGrid[i, j] - slopeMin) / slopeRange * 255);
                }
            }
            double[,] elevationGrid = GetDataRaster(groundDataPoints, GridType.GroundElevation, fileName, selectedArea);
            
            GetLowHigh(elevationGrid, out double zMin, out double zMax);
            //Calculate the range for the elevation
            double zRange = zMax - zMin;
            Console.WriteLine($"Z range ground data: {zRange}");

            double[,] redValueGrid = new double[selectedArea.XDistance, selectedArea.YDistance];
            for (int i = 0; i < selectedArea.XDistance; i++)
            {
                for (int j = 0; j < selectedArea.YDistance; j++)
                {
                    double elevationColorValue = (elevationGrid[i, j] - zMin) / zRange * 255;
                    double slopeColorValue = slopeGrid[i, j];
                    redValueGrid[i, j] = slopeColorValue + elevationColorValue * 0.2;
                }
            }
            GetLowHigh(redValueGrid, out double redValueMin, out double redValueMax);
            double redValueRange = redValueMax - redValueMin;
            for (int i = 0; i < selectedArea.XDistance; i++)
            {
                for (int j = 0; j < selectedArea.YDistance; j++)
                {
                    image.SetPixel(i, j, Color.FromArgb((int)((redValueGrid[i, j] - redValueMin) / redValueRange * 255), (int)(slopeGrid[i, j]), (int)(slopeGrid[i, j])));
                }
            }
        }

        private static void PixelSlopeColoring(DataPoint[] dataPoints, ref Bitmap image, string fileName, SelectedArea selectedArea, SlopeDirection slopeDirection, GridType gridType)
        {
            List<DataPoint> selectedDataPoints;
            if (gridType == GridType.GroundElevation) 
                SplitDataClasses(dataPoints, out _, out selectedDataPoints, out _, out _, out _);
            else //if (gridType == GridType.unidentifiedElevation)
                SplitDataClasses(dataPoints, out selectedDataPoints, out _, out _, out _, out _);
            double[,] slopeGrid = GetSlopeGrid(selectedDataPoints, fileName, selectedArea, slopeDirection, gridType);
            GetLowHigh(slopeGrid, out double slopeMin, out double slopeMax);
            double slopeRange = slopeMax - slopeMin;
            //Color the pixel corresponding to the DataPoint based on how high/low it is in the zRange (higher is brighter).
            for (int i = 0; i < slopeGrid.GetLength(0); i++)
            {
                for (int j = 1; j < slopeGrid.GetLength(1); j++)
                {
                    int value = (int)(255/(slopeGrid[i, j] + 1));
                    image.SetPixel(i, j, Color.FromArgb(value, value, value));
                    //In GUI, have slider for changing the coefficient (60)
                }
            }
        }
    }
}