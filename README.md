# LaserDataImage
This program uses LiDAR data in .las or .laz format to create various images. These can of course be used to just visualise the data, but it can also be used in combination with software like Open Orienteering Mapper. LaserDataImage outputs world files used for geo-referencing together with the PNGs, so they can be easily imported as templates in Mapper. It's also able to create DXF files, which can be imported to Open Orienteering Mapper as editable contours.

## Dependencies

LaserDataImage is built on .NET 5. It also requires [Las2Txt](https://github.com/LAStools/LAStools) for converting . If you're using *GNU/Linux*, please install las2txt/add it to PATH. If you're on *Windows*, please place the Las2Txt executable (las2txt.exe) in the same directory as the LaserDataImage binary.

## Compiling
With .NET 5 installed into PATH, run:

    $ dotnet run

## Usage

There are three ways of using the software. The GUI comes as a separate program, which can be found [here](https://gitlab.com/fonixord/laserdataimagegui/).

### Interactively in console

By compiling and running the main program (dotnet run), or running a pre-compiled binary, without any arguments, you will be guided through the process of choosing LiDAR files, image type and more. You will be asked about this for every file, allowing each image to be of a different type.

### Arguments in console

It is possible to run the whole program with one command. The possible arguments can be found by using --help. This is the recommended method if processing multiple files. All images will be of the same type. The command below creates DXF and PNG files for all the LiDAR files in the ./lidarFiles directory.

    $ dotnet run --folder --contours

### Graphical wrapper

The easiest method, however also the least transparent and least updated one, is the graphical wrapper. The GUI runs the main program using the arguments mode, hence both are required. At the moment, you may find that some options (like batch processing folders) are missing. Simply run the LaserDataImageGUI binary or compile and run it yourself, with the LaserDataImage binary in the same directory:

    $ dotnet run

## Image types

### Class

Assuming that the data points are classified, an image can be created showing these classes. 

![](docs/images/classes.png)

### Elevation

The image will be colored with a brighter color for a higher elevation.

![](docs/images/elevation.png)

### Custom elevation layers

The image will be colored brighter for each elevation layer. Eg: Every 5 meters will have the same colors. **This image type is currently missing settings for the height of each layer.**

![](docs/images/layers.png)

### Contour lines

Create an image and a DXF file showing the elevation as contour lines.

![](docs/images/contourLines.png)

### Unified elevation and class

Color the image with the class instead of the elevation when the point is not classified as a ground point.

![](docs/images/classElevation.png)

### Intensity

Color the image with the intensity value, where a higher intensity is a lighter color.

![](docs/images/intensity.png)

### Slope

Color the image using the slope (difference in elevation between two points), where a larger slope is a brighter color. This can either be a sum for both x and y:

![](docs/images/slopeSum.png)

Or just in the x direction:

![](docs/images/slopeLeft.png)

Or just in the y direction:

![](docs/images/slopeDown.png)

### Slope and elevation

Color the image brighter for steeper terrain and more red for higher elevation.

![](docs/images/slopeElevation.png)

### Slope of unidentified data

Like the slope sum, but for unidentified data.

![](docs/images/slopeUnidentified.png)

### Vegetation elevation

Color the image based on the elevation difference between the unclassified data points (such as vegetation) and the ground data points.

![](docs/images/vegetationElevation.png)