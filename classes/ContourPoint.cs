using System;

namespace LaserDataImage.classes
{
    public readonly struct ContourPoint : IComparable<ContourPoint>
    {
        public ContourPoint(double x, double y, double z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }
        public double X { get; }
        public double Y { get; }
        public double Z { get; }
        public int CompareTo(ContourPoint cp)
        {
            return (X + Y + Z).CompareTo(cp.X + cp.Y + cp.Z);
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != typeof(ContourPoint))
                return false;
            ContourPoint e = (ContourPoint)obj;
            return Math.Abs(e.X - X) < 0.00000000001 && Math.Abs(e.Y - Y) < 0.00000000001 && Math.Abs(e.Z - Z) < 0.00000000001;
        }
    }
}