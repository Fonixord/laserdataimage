using System.Collections.Generic;

namespace LaserDataImage.classes
{
    public class ContourLine
    {
        private List<ContourPoint> _contourPoints = new List<ContourPoint>();
        private int[] _firstEnd;
        private int[] _lastEnd;
            
        public ContourLine(ContourPoint start, ContourPoint end, int initialX, int initialY)
        {
            _contourPoints.Add(start);
            _contourPoints.Add(end);
            this._firstEnd = new int[] {initialX, initialY};
            this._lastEnd = new int[] {initialX, initialY};
        }
        public List<ContourPoint> ContourPoints
        {
            get { return _contourPoints; }
            set { _contourPoints = value; }
        }
        public int[] FirstEnd
        {
            get { return _firstEnd; }
            set { _firstEnd = value; }
        }
        public int[] LastEnd
        {
            get { return _lastEnd; }
            set { _lastEnd = value; }
        }
        public ContourPoint GetFirst()
        {
            return _contourPoints[0];
        }
        public ContourPoint GetLast()
        {
            return _contourPoints[^1];
        }
        public void Append(ContourPoint d)
        {
            _contourPoints.Add(d);
        }
        /*public void Append(ContourPoint d)
        {
            contourPoints.Insert(0, d);
        }*/
        public void Append(ContourLine cl)
        {
            _contourPoints.AddRange(cl.ContourPoints);
            _lastEnd = cl._lastEnd;
        }
        /*public void AddFirst(ContourLine cl)
        {
            contourPoints.InsertRange(0, cl.ContourPoints);
            firstEnd = cl.firstEnd;
        }*/
        public ContourLine RemoveFirst()
        {
            _contourPoints = _contourPoints.GetRange(1, _contourPoints.Count - 1);
            return this;
        }
        public ContourLine RemoveLast()
        {
            _contourPoints = _contourPoints.GetRange(0, _contourPoints.Count - 1);
            return this;
        }
        public int Count()
        {
            return _contourPoints.Count;
        }
        public void Reverse()
        {
            ContourPoints.Reverse();    //Reverse points
            (_firstEnd, _lastEnd) = (_lastEnd, _firstEnd);  //Reverse ends
        }
        public override bool Equals(object obj)
        {
            if (obj is null || obj.GetType() != typeof(ContourLine))
                return false;
            return _contourPoints == (obj as ContourLine)._contourPoints;
        }
    }
}