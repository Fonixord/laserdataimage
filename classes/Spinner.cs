namespace LaserDataImage.classes {
    using System.Threading;
    using System;
    public class Spinner : IDisposable {
        private readonly char[] _spinChars = new char[]{'|','/','—','\\'};
        private readonly Thread _thread;
        bool _enabled;
        public Spinner() {
            _thread = new Thread(Spin);
        }
        public void Start() {
            if (!_thread.IsAlive) {
                _thread.Start();
                _enabled = true;
            }
        }
        private void Spin() {
            int i = 0;
            Console.CursorVisible = false;
            while (_enabled) {
                Console.Write(_spinChars[i++%4]);    //Calculate which iteration it's on while incrementing i.
                if(Console.CursorLeft > 0)
                    Console.CursorLeft = Console.CursorLeft - 1;
                Thread.Sleep(500);
            }
        }
        public void Dispose() {
            Stop();
        }
        private void Stop(){
            if (_enabled)
            {
                _enabled = false;
                //Clear the spinner
                Console.Write(" ");
                Console.CursorVisible = true;
            }
        }
    }
}