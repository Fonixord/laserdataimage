using System;

namespace LaserDataImage.classes
{
    public class SelectedArea
    {
        public int MinX, MinY, MaxX, MaxY;
        public double DataXMin, DataYMin, DataXMax, DataYMax;

        public SelectedArea(double dataXMin, double dataYMin, double dataXMax, double dataYMax, double minX, double minY, double maxX, double maxY)
        {
            this.DataXMin = dataXMin;
            this.DataYMin = dataYMin;
            this.DataXMax = dataXMax;
            this.DataYMax = dataYMax;
            if(minX == 0 && minY == 0 && maxX == 0 && maxY == 0){   //  To be interpreted that the whole dataset should be processed
                this.MinX = 0;
                this.MinY = 0;
                this.MaxX = (int)Math.Round(dataXMax - dataXMin);
                this.MaxY = (int)Math.Round(dataYMax - dataYMin);
            } else {
                this.MinX = (int)Math.Round(minX);
                this.MinY = (int)Math.Round(minY);
                this.MaxX = (int)Math.Round(maxX);
                this.MaxY = (int)Math.Round(maxY);
            }
        }

        public int XDistance
        {
            get { return MaxX - MinX; }
        }

        public int YDistance
        {
            get { return MaxY - MinY; }
        }

        public int DataXDistance
        {
            get { return (int)Math.Round(DataXMax - DataXMin); }
        }

        public int DataYDistance
        {
            get { return (int)Math.Round(DataYMax - DataYMin); }
        }
        public bool IsFullSize()
        {
            return XDistance == DataXDistance && YDistance == DataYDistance;
        }
    }
}