namespace LaserDataImage.classes
{
    public class PastContourPoint
    {
        public PastContourPoint(double x, double h){
            X = x;
            H = h;
        }
        public double X { get; set; }
        public double H { get; set; }
    }
}