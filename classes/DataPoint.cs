using System;
using System.Collections;
using System.Collections.Generic;
using CsvHelper.Configuration.Attributes;

namespace LaserDataImage.classes
{
    public class DataPoint : IComparable <DataPoint>
    {
        public DataPoint(double x, double y, double z, ushort c, ushort i){
            this.X = x;
            this.Y = y;
            this.Z = z;
            this.C = c;
            this.I = i;
        }
        //Sort by smallest X first.
        private class SortXHelper : IComparer{
            int IComparer.Compare(object a, object b)
            {
                if (a is null || b is null)
                    return 0;
                DataPoint dataPointA = (DataPoint)a;
                DataPoint dataPointB = (DataPoint)b;

                if(dataPointA.X < dataPointB.X)
                    return 1;
                if(dataPointA.X > dataPointB.X)
                    return -1;
                else 
                    return 0;
            }
        }
        //Sort by smallest Y first.
        private class SortYHelper : IComparer{
            int IComparer.Compare(object a, object b){
                if (a is null || b is null)
                    return 0;
                DataPoint dataPointA = (DataPoint)a;
                DataPoint dataPointB = (DataPoint)b;

                if(dataPointA.Y < dataPointB.Y)
                    return 1;
                if(dataPointA.Y > dataPointB.Y)
                    return -1;
                else 
                    return 0;
            }
        }
        //Smallest distance first
        private class SortDistanceHelper : IComparer<DataPoint>{
            int IComparer<DataPoint>.Compare(DataPoint a, DataPoint b){
                if (a is null || b is null)
                    return 0;
                if(a.Distance > b.Distance)
                    return 1;
                if(a.Distance < b.Distance)
                    return -1;
                return 0;
            }
        }
        [field: Index(0)]
        public double X { get; }

        [field: Index(1)]
        public double Y { get; }

        [field: Index(2)]
        public double Z { get; }

        [field: Index(3)]
        public ushort C { get; }

        [field: Index(4)]
        public ushort I { get; }

        public double Distance { get; set; }
        //Default comparer (X, smallest first)
        public int CompareTo(DataPoint d){
            return d.X.CompareTo(X);
        }

        public static IComparer SortX(){
            return new SortXHelper();
        }

        public static IComparer SortY(){
            return new SortYHelper();
        }

        public static IComparer<DataPoint> SortDistance(){
            return new SortDistanceHelper();
        }

        public override string ToString(){
            return X + "," + Y + "," + Z + "," + C;
        }
    }
}